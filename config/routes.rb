Rails.application.routes.draw do
  root 'list#index'

  get 'get_object', to: 'list#get_object', as: 'imagen'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
