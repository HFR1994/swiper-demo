class ListController < ApplicationController
  def index
    @perro = Perro.all
  end

  def get_object

    # noinspection RubyArgCount
    s3 = Aws::S3::Client.new

    if params[:value].nil?
      send_data open("#{Rails.root}/public/question.jpg", "rb") { |f| f.read }, :filename => 'default.jpg', type: 'image/jpeg', disposition: 'inline'
    else
      resp = s3.get_object(bucket: 'fotos-perros',key: params[:value])
      send_data(resp.body.read,
                :filename => params[:value],
                :type => 'image/jpeg',
                :disposition => 'inline')
    end
  end
end
